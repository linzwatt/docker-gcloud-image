FROM docker:latest

# Set SDK directory
ENV GCLOUD_SDK_DIR=/usr/local/lib

# Install dependencies
RUN apk update && apk add --no-cache curl python3 ca-certificates

# Download and extract Google Cloud CLI
RUN curl -o google-cloud-cli.tar.gz https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-cli-469.0.0-linux-x86_64.tar.gz && \
    tar -xf google-cloud-cli.tar.gz -C $GCLOUD_SDK_DIR && \
    rm google-cloud-cli.tar.gz

# Install Google Cloud CLI
RUN $GCLOUD_SDK_DIR/google-cloud-sdk/install.sh --quiet

# Adding Google Cloud SDK to PATH
ENV PATH=$GCLOUD_SDK_DIR/google-cloud-sdk/bin:$PATH

# Verify installation
RUN gcloud version
